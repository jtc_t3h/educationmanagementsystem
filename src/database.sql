-- Tao database #####################################################
create database ems;

use ems;

-- Tao table #######################################################
create table subjects_group(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(50),
  primary key (id)
);
create index subjects_group_idx on subjects_group(id);

insert into subjects_group(name) values ('Programing');
insert into subjects_group(name) values ('Digital Marketing');
insert into subjects_group(name) values ('Network');

create table subjects(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100),
  group_id int,
  duration int,
  description blob,
  primary key (id),
  FOREIGN KEY (id) REFERENCES subjects_group(id)
);
create index subjects_idx on subjects(id);

insert into subjects(name, group_id, duration, description) values ('Lap trinh Java - Module 1', 1, 30, 'Lap trinh Java - Module 1');
insert into subjects(name, group_id, duration, description) values ('Facebook markting', 2, 30, 'Facebook markting');
insert into subjects(name, group_id, duration, description) values ('Cisco - Module 1', 3, 30, 'Cisco - Module 1');



select s.id, s.name, g.name group_id, s.duration, s.descritpion 
from subjects s, subjects_group g 
where s.group_id = g.id;


select s.id, s.name, g.name group_id, s.duration, s.description 
from subjects s, subjects_group g 
where s.group_id = g.id
and (s.id = 1 or s.name like '%1%' or g.name like '%1%' or s.duration = 1 or s.description like '%1%');







