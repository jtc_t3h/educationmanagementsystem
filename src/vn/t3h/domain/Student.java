package vn.t3h.domain;

import java.sql.Date;

public class Student {

	// field - properties: thể hiện trạng thái của đối tượng -> instance variable
	private Long id;
	private String fullName;
	private String mobiNumber;
	private Date birthday;
	private String email;
	
	// Hàm khởi tạo có 1 args
	public Student(Long _id) {
		id = _id;
	}
	
	// Hàm khởi tạo mặc định tường minh
	public Student() {
	}

	public Student(Long id, String fullName) {
		this.id = id;
		this.fullName = fullName;
	}
	
	// setter
	public void setId(Long id) {
		this.id = id;
	}
	// getter
	public Long getId() {
		return id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMobiNumber() {
		return mobiNumber;
	}

	public void setMobiNumber(String mobiNumber) {
		this.mobiNumber = mobiNumber;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String display() {
		return id + "  " + fullName;
	}
}
