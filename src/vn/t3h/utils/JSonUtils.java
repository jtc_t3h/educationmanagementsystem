package vn.t3h.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import vn.t3h.domain.Subjects;

public class JSonUtils {

	public static List<Subjects> readSubject(File fileSelected) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		Gson gson = new Gson();
		Subjects[] arrSubjects = gson.fromJson(new FileReader(fileSelected), Subjects[].class);
		return Arrays.asList(arrSubjects);
	}

}
