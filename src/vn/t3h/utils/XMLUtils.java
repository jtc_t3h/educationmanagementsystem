package vn.t3h.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import vn.t3h.domain.Subjects;

public class XMLUtils {
	
	public static List<Subjects> readSubject(File fileSelected) throws ParserConfigurationException, SAXException, IOException  {
		List<Subjects> listOfSubjects = new ArrayList<Subjects>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(fileSelected);
				
		NodeList subjectsNodeList = document.getElementsByTagName("subjects");
		for (int idx = 0; idx < subjectsNodeList.getLength(); idx++) {
			Subjects subjects = new Subjects();
			Node subjectsNode = subjectsNodeList.item(idx);
			
			NodeList children = subjectsNode.getChildNodes();
			for (int index = 0; index < children.getLength(); index++) {
				Node child = children.item(index);
				if ("id".equals(child.getNodeName())) {
					subjects.setId(child.getTextContent());
				} else if ("name".equals(child.getNodeName())) {
					subjects.setName(child.getTextContent());
				} else if ("group".equals(child.getNodeName())) {
					subjects.setGroup(child.getTextContent());
				} else if ("duration".equals(child.getNodeName())) {
					subjects.setDuration(Integer.parseInt(child.getTextContent()));
				} else if ("description".equals(child.getNodeName())) {
					subjects.setDescription(child.getTextContent());
				}
			}
			
//			Element subjectsNode2 = (Element) subjectsNode;
//			subjects.setId(subjectsNode2.getElementsByTagName("id").item(0).getTextContent());
//			subjects.setName(subjectsNode2.getElementsByTagName("id").item(0).getTextContent());
//			subjects.setGroup(subjectsNode2.getElementsByTagName("id").item(0).getTextContent());
//			subjects.setDuration(subjectsNode2.getElementsByTagName("id").item(0).getTextContent());
//			subjects.setDescription(subjectsNode2.getElementsByTagName("id").item(0).getTextContent());
			
			listOfSubjects.add(subjects);
		}
		
		return listOfSubjects;
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document document = builder.parse(new File("F:\\Workspace/subject_import.xml"));
		
		Element listNode = document.getDocumentElement();
		System.out.println(listNode.getNodeName());
		
		NodeList listChildren = listNode.getChildNodes();
		System.out.println(listChildren.getLength());
		for (int idx = 0; idx < listChildren.getLength(); idx++) {
			Node childNode = listChildren.item(idx);
			
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				System.out.println(childNode.getNodeName());
				
				Node subjectsNode = childNode;
				NodeList subjectsChildren = subjectsNode.getChildNodes();
			}
			
//			if ("subjects".equals(childNode.getNodeName())){
//				
//			}
		}
	}

}
