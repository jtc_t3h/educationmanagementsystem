package vn.t3h.utils;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import vn.t3h.gui.MainGUI;
import vn.t3h.gui.SubjectsGUI;

public class MultiLanguageUtils {
	
	public static ResourceBundle res;
	private static List<Container> containers = new ArrayList<Container>();
	
	static {
		setBundle("en", "US");
	}
	
	public static void setBundle(String languageCode, String countryCode) {
		Locale locale = new Locale(languageCode, countryCode);
		res = ResourceBundle.getBundle("vn.t3h.bundle.ApplicationResources", locale);
		
		containers.forEach(container ->{
			if (container instanceof MainGUI) {
				((MainGUI) container).initText();
			} else if (container instanceof SubjectsGUI) {
				((SubjectsGUI)container).initText();
			}
		});
	}
	
	
	public static void addContainer(Container container) {
		containers.add(container);
	}
	public static void removeContainer(Container container) {
		containers.remove(container);
	}
	
	

	public static ResourceBundle getBundle(String languageCode, String countryCode) {
		Locale locale = new Locale(languageCode, countryCode);
		return ResourceBundle.getBundle("vn.t3h.bundle.ApplicationResources", locale);
	}
	
	public static void main(String[] args) {
		String languageCode = "en";
		String countryCode = "US";
		
		ResourceBundle res = getBundle(languageCode, countryCode);
		System.out.println(res.getString("courses"));
		System.out.println(res.getString("dashboard"));
	}
	
	
}
