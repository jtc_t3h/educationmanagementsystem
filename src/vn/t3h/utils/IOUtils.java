package vn.t3h.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import vn.t3h.domain.Subjects;

public class IOUtils {

	/**
	 *  Read file 
	 * @param fileSelected
	 * 			with CSV format
	 * @return 
	 * 		List of Subject
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static List<Subjects> readSubject(File fileSelected) throws FileNotFoundException, IOException {
		List<Subjects> list = new ArrayList<Subjects>();
		if (fileSelected != null) {
			String content = "";
			try (FileReader in = new FileReader(fileSelected)){
				int c;
				while ((c = in.read()) != -1){
					content += (char)c;
				}
			}
			
			// split row
			StringTokenizer st = new StringTokenizer(content, "\n");
			while (st.hasMoreTokens()) {
				String line = st.nextToken();
				
				// convert line to subject object
				String[] columns = line.split(",");
				Subjects subjects = new Subjects();
				subjects.setId(columns[0].trim());
				subjects.setName(columns[1].trim());
				subjects.setGroup(columns[2].trim());
				subjects.setDuration(Integer.parseInt(columns[3].trim()));
				subjects.setDescription(columns[4].trim());
				
				list.add(subjects);
			}
		}		
		return list;
	}

}
