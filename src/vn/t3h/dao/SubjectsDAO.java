package vn.t3h.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import vn.t3h.domain.Subjects;

public class SubjectsDAO {

	public List<Subjects> findAll() {
		List<Subjects> list = new ArrayList<Subjects>();

		String URL = "jdbc:mysql://localhost:3306/ems";
		String username = "root";
		String password = "";
		
		String selectSQL = "select s.id, s.name, g.name group_id, s.duration, s.description from subjects s, subjects_group g where s.group_id = g.id";
		try (Connection con = DriverManager.getConnection(URL, username, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(selectSQL)){
			
			while(rs.next()) {
			   Subjects subjects = new Subjects();
			   
			   subjects.setId(String.valueOf(rs.getInt("id")));
			   subjects.setName(rs.getString("name"));
			   subjects.setGroup(rs.getString("group_id"));
			   subjects.setDuration(rs.getInt("duration"));
			   subjects.setDescription(rs.getString("description"));
			   
			   list.add(subjects);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public List<Subjects> findByText(String text){
		if (text.isEmpty()) {
			return findAll();
		}
		
		List<Subjects> list = new ArrayList<Subjects>();

		String URL = "jdbc:mysql://localhost:3306/ems";
		String username = "root";
		String password = "";
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select s.id, s.name, g.name group_id, s.duration, s.description ");
		sb.append(" from subjects s, subjects_group g ");
		sb.append(" where s.group_id = g.id ");
		sb.append(" and (s.id = " + text + " or s.name like '%" + text + "%' or g.name like '%" + text + "%' or s.duration = " + text + " or s.description like '%" + text + "%')" );
		try (Connection con = DriverManager.getConnection(URL, username, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(sb.toString())){
			
			while(rs.next()) {
			   Subjects subjects = new Subjects();
			   
			   subjects.setId(String.valueOf(rs.getInt("id")));
			   subjects.setName(rs.getString("name"));
			   subjects.setGroup(rs.getString("group_id"));
			   subjects.setDuration(rs.getInt("duration"));
			   subjects.setDescription(rs.getString("description"));
			   
			   list.add(subjects);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
}
