package vn.t3h.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class CoursesGUI extends JPanel {
	private JTextField txtSearch;
	private JTable table;

	/**
	 * Create the panel.
	 */
	public CoursesGUI() {
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 946, 34);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblCourses = new JLabel("Courses");
		lblCourses.setIcon(new ImageIcon(CoursesGUI.class.getResource("/vn/t3h/resources/timetable.png")));
		lblCourses.setBounds(6, 6, 87, 16);
		panel.add(lblCourses);
		
		JLabel lblAdd = new JLabel("");
		lblAdd.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdd.setIcon(new ImageIcon(CoursesGUI.class.getResource("/vn/t3h/resources/plus.png")));
		lblAdd.setBounds(244, 0, 32, 34);
		panel.add(lblAdd);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(CoursesGUI.class.getResource("/vn/t3h/resources/document.png")));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(284, 0, 32, 34);
		panel.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(CoursesGUI.class.getResource("/vn/t3h/resources/x-button.png")));
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(328, 0, 32, 34);
		panel.add(label_1);
		
		txtSearch = new JTextField();
		
		txtSearch.setBounds(706, -2, 234, 33);
		panel.add(txtSearch);
		txtSearch.setColumns(10);
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSearch.setBounds(628, 3, 66, 22);
		panel.add(lblSearch);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 34, 946, 456);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}
}
