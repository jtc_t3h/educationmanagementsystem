package vn.t3h.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import vn.t3h.domain.Subjects;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class SubjectsEditGUI extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtID;
	private JTextField txtName;
	private JTextField txtDuration;

	private boolean flg = false;
	
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			SubjectsAddGUI dialog = new SubjectsAddGUI();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public SubjectsEditGUI(Subjects subjects) {
		setTitle("Add a new Subject");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("ID");
			lblNewLabel.setBounds(18, 16, 112, 22);
			contentPanel.add(lblNewLabel);
		}
		
		txtID = new JTextField();
		txtID.setBounds(142, 14, 302, 26);
		txtID.setEditable(false);
		txtID.setText(subjects.getId());
		contentPanel.add(txtID);
		txtID.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(18, 52, 112, 22);
		contentPanel.add(lblName);
		
		txtName = new JTextField();
		txtName.setColumns(10);
		txtName.setBounds(142, 50, 302, 26);
		txtName.setText(subjects.getName());
		contentPanel.add(txtName);
		
		JLabel lblGroup = new JLabel("Group");
		lblGroup.setBounds(18, 87, 112, 22);
		contentPanel.add(lblGroup);
		
		JLabel lblDuration = new JLabel("Duration");
		lblDuration.setBounds(18, 123, 112, 22);
		contentPanel.add(lblDuration);
		
		txtDuration = new JTextField();
		txtDuration.setColumns(10);
		txtDuration.setBounds(142, 121, 302, 26);
		txtDuration.setText(String.valueOf(subjects.getDuration()));
		contentPanel.add(txtDuration);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(18, 159, 112, 22);
		contentPanel.add(lblDescription);
		
		JComboBox cbbGroup = new JComboBox();
		cbbGroup.setModel(new DefaultComboBoxModel(new String[] {"--- Select ---", "Lap Trinh Java"}));
		cbbGroup.setBounds(142, 88, 302, 27);
		cbbGroup.setSelectedItem(subjects.getGroup());
		contentPanel.add(cbbGroup);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(142, 159, 302, 74);
		contentPanel.add(scrollPane);
		
		JTextArea taDescription = new JTextArea();
		taDescription.setText(subjects.getDescription());
		scrollPane.setViewportView(taDescription);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnAdd = new JButton("Save");
				btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						subjects.setId(txtID.getText().trim());
						subjects.setName(txtName.getText().trim());
						subjects.setGroup(cbbGroup.getSelectedItem().toString());
						subjects.setDuration(Integer.parseInt(txtDuration.getText().trim()));
						subjects.setDescription(taDescription.getText().trim());
						
						flg = true;
						dispose();
					}
				});
				btnAdd.setActionCommand("OK");
				buttonPane.add(btnAdd);
				getRootPane().setDefaultButton(btnAdd);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	public boolean isFlg() {
		return flg;
	}
}
