package vn.t3h.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import vn.t3h.utils.MultiLanguageUtils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import java.awt.CardLayout;

public class MainGUI extends JFrame {

	private JPanel contentPane;
	private JPanel panelMain;
	private JLabel lblDashboard;
	private JLabel lblCourses;
	private JLabel lblAmerican;
	private JLabel lblVietnam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
//					DlgInitProgress dlg = new DlgInitProgress();
//					dlg.setVisible(true);
//					Thread th = new Thread(dlg);
//					th.start();
//					th.join();
					
					MainGUI frame = new MainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainGUI() {
		MultiLanguageUtils.addContainer(this);
		
		setTitle("Education Management System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 939, 651);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(0, 0, 939, 69);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblCourses = new JLabel(MultiLanguageUtils.res.getString("courses"));
		lblCourses.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				((CardLayout)panelMain.getLayout()).show(panelMain, "courses");
			}
		});
		lblCourses.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/timetable.png")));
		lblCourses.setHorizontalAlignment(SwingConstants.CENTER);
		lblCourses.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCourses.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblCourses.setBounds(79, 6, 61, 52);
		panel.add(lblCourses);
		
		lblDashboard = new JLabel(MultiLanguageUtils.res.getString("dashboard"));
		lblDashboard.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/home.png")));
		lblDashboard.setHorizontalTextPosition(SwingConstants.CENTER);
		lblDashboard.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblDashboard.setBounds(6, 6, 68, 52);
		panel.add(lblDashboard);
		
		JLabel lblClasses = new JLabel("Classes");
		lblClasses.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				((CardLayout)panelMain.getLayout()).show(panelMain, "classes");
			}
		});
		lblClasses.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/classroom.png")));
		lblClasses.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblClasses.setHorizontalTextPosition(SwingConstants.CENTER);
		lblClasses.setHorizontalAlignment(SwingConstants.CENTER);
		lblClasses.setBounds(152, 6, 61, 52);
		panel.add(lblClasses);
		
		JLabel lblStudents = new JLabel("Students");
		lblStudents.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				((CardLayout)panelMain.getLayout()).show(panelMain, "student");
			}
		});
		lblStudents.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/graduate-student-avatar.png")));
		lblStudents.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblStudents.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStudents.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudents.setBounds(225, 6, 61, 52);
		panel.add(lblStudents);
		
		JLabel lblLogout = new JLabel("Log out");
		lblLogout.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/logout.png")));
		lblLogout.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblLogout.setHorizontalTextPosition(SwingConstants.CENTER);
		lblLogout.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogout.setBounds(872, 6, 61, 52);
		panel.add(lblLogout);
		
		JLabel lblSubjects = new JLabel("Subjects");
		lblSubjects.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				((CardLayout)panelMain.getLayout()).show(panelMain, "subjects");
			}
		});
		lblSubjects.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/language.png")));
		lblSubjects.setBounds(302, 6, 61, 52);
		lblSubjects.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblSubjects.setHorizontalTextPosition(SwingConstants.CENTER);
		lblSubjects.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblSubjects);
		
		lblAmerican = new JLabel("");
		lblAmerican.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				lblAmerican_mouseClicked(arg0);
			}
		});
		lblAmerican.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/america.png")));
		lblAmerican.setBounds(778, 18, 25, 18);
		panel.add(lblAmerican);
		
		lblVietnam = new JLabel("");
		lblVietnam.setIcon(new ImageIcon(MainGUI.class.getResource("/vn/t3h/resources/vietnam.png")));
		lblVietnam.setBounds(813, 18, 25, 18);
		lblVietnam.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblVietnam_mouseClicked(e);
			}
		});
		panel.add(lblVietnam);
		
		panelMain = new JPanel();
		panelMain.setBounds(0, 71, 939, 507);
		contentPane.add(panelMain);
		panelMain.setLayout(new CardLayout(0,0));
		
		JLabel lblUserAdministrator = new JLabel("User: Administrator");
		lblUserAdministrator.setHorizontalAlignment(SwingConstants.LEFT);
		lblUserAdministrator.setBounds(10, 585, 286, 16);
		contentPane.add(lblUserAdministrator);
		
		JLabel lblNewLabel = new JLabel("Friday, 2 Aug 2019");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(663, 585, 270, 16);
		contentPane.add(lblNewLabel);
		
		initText();
		initCartLayOut();
	}

	protected void lblVietnam_mouseClicked(MouseEvent e) {
		lblVietnam.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblAmerican.setBorder(null);
		
		MultiLanguageUtils.setBundle("vi", "VN");
//		initText();
	}

	public void initText() {
		lblCourses.setText(MultiLanguageUtils.res.getString("courses"));
		lblDashboard.setText(MultiLanguageUtils.res.getString("dashboard"));
		
	}

	protected void lblAmerican_mouseClicked(MouseEvent arg0) {
		lblAmerican.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblVietnam.setBorder(null);
		
		MultiLanguageUtils.setBundle("en", "US");
//		initText();
	}

	private void initCartLayOut() {
		DashboardGUI dashboard = new DashboardGUI();
		panelMain.add(dashboard, "dashboard");
		
		CoursesGUI courses = new CoursesGUI();
		panelMain.add(courses, "courses");
		
		ClassesGUI classes = new ClassesGUI();
		panelMain.add(classes, "classes");
		
		StudentsGUI student = new StudentsGUI();
		panelMain.add(student, "student");
		
		SubjectsGUI subjects = new SubjectsGUI();
		panelMain.add(subjects, "subjects");
		
	}
}
