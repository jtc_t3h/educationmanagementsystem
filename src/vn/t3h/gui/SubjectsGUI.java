package vn.t3h.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import vn.t3h.dao.SubjectsDAO;
import vn.t3h.domain.Subjects;
import vn.t3h.utils.IOUtils;
import vn.t3h.utils.JSonUtils;
import vn.t3h.utils.MultiLanguageUtils;
import vn.t3h.utils.XMLUtils;

import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTable;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class SubjectsGUI extends JPanel {
	private JTextField txtSearch;
	private JTable table;
	private JLabel lblSubjects;

	/**
	 * Create the panel.
	 */
	public SubjectsGUI() {
		MultiLanguageUtils.addContainer(this);
		
		setLayout(null);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 946, 34);
		add(panel);

		lblSubjects = new JLabel("Subjects");
		lblSubjects.setIcon(new ImageIcon(SubjectsGUI.class.getResource("/vn/t3h/resources/language.png")));
		lblSubjects.setBounds(6, 6, 94, 22);
		panel.add(lblSubjects);

		JLabel lblAdd = new JLabel("");
		lblAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblAdd_mouseClicked(e);
			}
		});
		lblAdd.setIcon(new ImageIcon(SubjectsGUI.class.getResource("/vn/t3h/resources/plus.png")));
		lblAdd.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdd.setBounds(244, 0, 32, 34);
		panel.add(lblAdd);

		JLabel lblEdit = new JLabel("");
		lblEdit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				lblEdit_mouseClicked(arg0);
			}
		});
		lblEdit.setIcon(new ImageIcon(SubjectsGUI.class.getResource("/vn/t3h/resources/document.png")));
		lblEdit.setHorizontalAlignment(SwingConstants.CENTER);
		lblEdit.setBounds(284, 0, 32, 34);
		panel.add(lblEdit);

		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(SubjectsGUI.class.getResource("/vn/t3h/resources/x-button.png")));
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setBounds(328, 0, 32, 34);
		panel.add(label_3);

		txtSearch = new JTextField();
		txtSearch.setColumns(10);
		txtSearch.setBounds(706, -2, 234, 33);
		panel.add(txtSearch);
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				txtSearch_keyReleased(e);
			}
		});

		JLabel label_4 = new JLabel("Search");
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);
		label_4.setBounds(628, 3, 66, 22);
		panel.add(label_4);

		JLabel lblImport = new JLabel("");
		lblImport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblImport_mounseClicked(e);
			}
		});
		lblImport.setIcon(new ImageIcon(SubjectsGUI.class.getResource("/vn/t3h/resources/import.png")));
		lblImport.setHorizontalAlignment(SwingConstants.CENTER);
		lblImport.setBounds(419, 0, 32, 34);
		panel.add(lblImport);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 34, 946, 456);
		add(scrollPane);

		table = new JTable();
		initModelTable();
		scrollPane.setViewportView(table);

		// init data from table
		intData();
		initText();
	}

	public void initText() {
		lblSubjects.setText(MultiLanguageUtils.res.getString("subjects"));
		
	}

	protected void txtSearch_keyReleased(KeyEvent e) {
		String text = txtSearch.getText();
		
		SubjectsDAO dao = new SubjectsDAO();
		List<Subjects> listOfSubjects = dao.findByText(text.trim());
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		while (model.getRowCount() > 0) {
			model.removeRow(model.getRowCount() - 1);
		}
		for (Subjects subject : listOfSubjects) {
			model.addRow(new Object[] { subject.getId(), subject.getName(), subject.getGroup(), subject.getDuration(),
					subject.getDescription() });
		}
		
	}

	private void intData() {
		SubjectsDAO dao = new SubjectsDAO();
		List<Subjects> listOfSubjects = dao.findAll();

		// Add new row
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (Subjects subject : listOfSubjects) {
			model.addRow(new Object[] { subject.getId(), subject.getName(), subject.getGroup(), subject.getDuration(),
					subject.getDescription() });
		}
	}

	protected void lblEdit_mouseClicked(MouseEvent arg0) {
		// Lay dong duoc chon
		int idxSeleted = table.getSelectedRow();

		// Doi tuong luu tru thong tin dong duoc chon
		Subjects subject = new Subjects();
		subject.setId(table.getValueAt(idxSeleted, 0).toString());
		subject.setName(table.getValueAt(idxSeleted, 1).toString());
		subject.setGroup(table.getValueAt(idxSeleted, 2).toString());
		subject.setDuration(Integer.parseInt(table.getValueAt(idxSeleted, 3).toString()));
		subject.setDescription(table.getValueAt(idxSeleted, 4).toString());

		// Mo form
		SubjectsEditGUI gui = new SubjectsEditGUI(subject);
		gui.setLocationRelativeTo(this);
		gui.setVisible(true);

		//
		gui.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				if (gui.isFlg()) {
					table.setValueAt(subject.getId(), idxSeleted, 0);
					table.setValueAt(subject.getName(), idxSeleted, 1);
					table.setValueAt(subject.getGroup(), idxSeleted, 2);
					table.setValueAt(subject.getDuration(), idxSeleted, 3);
					table.setValueAt(subject.getDescription(), idxSeleted, 4);
				}
			}
		});

	}

	protected void lblAdd_mouseClicked(MouseEvent e) {
		//
		Subjects subject = new Subjects();

		// Show Add GUI
		SubjectsAddGUI addGUI = new SubjectsAddGUI(subject);
		addGUI.setLocationRelativeTo(this);
		addGUI.setVisible(true);
		addGUI.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				if (addGUI.isFlg()) {
					// add row to table data
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.addRow(new Object[] { subject.getId(), subject.getName(), subject.getGroup(),
							subject.getDuration(), subject.getDescription() });
				}
			}
		});
	}

	protected void lblImport_mounseClicked(MouseEvent e) {
		// select text file with format
		File fileSelected = null;

		JFileChooser fcs = new JFileChooser();
		if (fcs.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			fileSelected = fcs.getSelectedFile();
		}

		// Read file
		List<Subjects> listOfSubjects = null;
		try {
			if (fileSelected.getName().endsWith(".json")) {
				listOfSubjects = JSonUtils.readSubject(fileSelected);
			} else if (fileSelected.getName().endsWith(".xml")) {
				listOfSubjects = XMLUtils.readSubject(fileSelected);
			} else {
				listOfSubjects = IOUtils.readSubject(fileSelected);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e1) {
			e1.printStackTrace();
		}

		// reload model of table
		reloadModelTable(listOfSubjects);
	}

	private void reloadModelTable(List<Subjects> listOfSubjects) {
		// Delete current
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		while (model.getRowCount() > 0) {
			model.removeRow(model.getRowCount() - 1);
		}

		// Add new row
		for (Subjects subject : listOfSubjects) {
			model.addRow(new Object[] { subject.getId(), subject.getName(), subject.getGroup(), subject.getDuration(),
					subject.getDescription() });
		}
	}

	private void initModelTable() {
		Object[][] data = {};
		String[] header = { "ID", "Name", "Group", "Duration", "Description" };

		DefaultTableModel model = new DefaultTableModel(data, header);
		table.setModel(model);
	}
}
